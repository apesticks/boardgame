import pygame
from tools import pg_caption
from unit import Unit

class Player(object):

    def __init__(self, screen):
        self.screen = screen
        self.unit = Unit()

    def init_unit(self, default_unit_size = 32):
        self.unit.set(default_unit_size)
