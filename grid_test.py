import pygame
from tools import pg_caption

class Grid(object):

    def __init__(self, screen):
        self.screen = screen
        self.division = 50
        self.x_divisions = self.screen.get_size()[0]/self.division
        self.y_divisions = self.screen.get_size()[1]/self.division
        self.trigger = False
        self.point_list = []
        self.remove = False

        #remove delay
        self.delay = 0
        self.delay_init = self.delay

    def draw(self):
        for division in range(self.x_divisions):
            pygame.draw.line(self.screen, (0, 0, 0), (self.division*division, 0), (self.division*division, self.screen.get_size()[1]))

        for division in range(self.y_divisions):
            pygame.draw.line(self.screen, (0, 0, 0), (0, self.division*division), (self.screen.get_size()[0], self.division*division))

    def collect_points(self):
        if self.trigger == True:
            self.point_list.append((self.mouse_coords_x, self.mouse_coords_y))
            self.trigger = False

    def remove_points(self):
        if self.remove == True:
            if self.delay != 0:
                self.delay = self.delay - 1
            if self.delay == 0:
                if len(self.point_list) > 0:
                    self.point_list.pop(-1)
                self.delay = 10
        else:
            self.delay = 0
            
    def draw_points(self):
        if len(self.point_list) > 1:
            pygame.draw.lines(self.screen, (0, 0, 255), False, self.point_list, 2)

    def update(self):
        self.mouse_coords_x = (pygame.mouse.get_pos()[0]/self.division * self.division)
        self.mouse_coords_y = (pygame.mouse.get_pos()[1]/self.division * self.division)
        self.draw()
        self.collect_points()
        self.draw_points()
        self.remove_points()
        pg_caption(str(self.mouse_coords_x) + " " + str(self.mouse_coords_y) + " " + str(self.trigger) + " " + str(self.point_list))