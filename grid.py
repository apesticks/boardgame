import pygame
from tools import pg_caption

class Grid(object):

    def __init__(self, screen):
        self.screen = screen
        self.division = 50
        self.board_size = 13
        self.add = False
        self.temp = []

    def draw(self):
        for division in range(self.board_size):
            pygame.draw.line(self.screen, (255, 255, 255), (self.division*division + 100, 0), (self.division*division + 100, self.screen.get_size()[1]))

        for division in range(self.board_size):
            pygame.draw.line(self.screen, (255, 255, 255), (100, self.division*division), (self.screen.get_size()[0] - 100, self.division*division))

    def save_coords(self):
        if self.add == True:
            if self.mouse_coords_x > 100 and self.mouse_coords_x < self.screen.get_size()[0] - 100:
                self.temp.append([self.mouse_coords_x, self.mouse_coords_y])

    def draw_unit(self):
        for item in self.temp:
            pygame.draw.circle(self.screen, (0,0,255), (item[0], item[1]), 10, 10)
            
    def update(self):
        self.draw()
        self.mouse_coords_x = (pygame.mouse.get_pos()[0]/self.division * self.division) + 25
        self.mouse_coords_y = (pygame.mouse.get_pos()[1]/self.division * self.division) + 25
        self.save_coords()
        self.draw_unit()
        pg_caption(str(self.add))