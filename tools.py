import pygame

def pg_caption(*captions):
    cap = ''
    for caption in captions:
        cap = cap + str(caption) + " - "

    pygame.display.set_caption(cap)

def image_mapping(self, rows=0, items=0):
    image_map = []
    for row in range(rows):
	for item in range(items):
		image_map.append([self.pixel_x * item, self.pixel_y * row])
    return image_map	
