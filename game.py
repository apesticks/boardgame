import pygame, sys, os, pdb
from pygame.locals import *
from unit import Unit
from grid import Grid

class Game(object):

    def __init__(self, screen_size=(800,600)):
        pygame.init()
        self.fps = pygame.time.Clock()
        self.running = True

        #sets the screen to apear in same position everytime
        self.window_position = 790, 250
        os.environ["SDL_VIDEO_WINDOW_POS"] = str(self.window_position[0]) + "," + str(self.window_position[1])
        self.screen_size = screen_size
        self.screen = pygame.display.set_mode(self.screen_size ,0 , 32)

        #instantiations
        self.grid = Grid(self.screen)

    def run(self):
        while self.running:
            self.fps.tick(60)
            self.screen.fill((0,0,0))
            self.grid.update()
            self.events()
            pygame.display.update()

    def events(self):
        for event in pygame.event.get():
            self.game_quit_events(event)
            self.mouse_events(event)
            self.point_manager_events(event)

    def game_quit_events(self, event):
        if event.type == QUIT:
            self.running = False
        if event.type == KEYDOWN:
           if event.key == K_ESCAPE:
               self.running = False

    def mouse_events(self, event):
        if event.type == MOUSEBUTTONDOWN:
            self.grid.add = True
        if event.type == MOUSEBUTTONUP:
            self.grid.add = False

    def point_manager_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_BACKSPACE:
                self.grid.remove = True
       
        if event.type == KEYUP:
            if event.key == K_BACKSPACE:
                self.grid.remove = False